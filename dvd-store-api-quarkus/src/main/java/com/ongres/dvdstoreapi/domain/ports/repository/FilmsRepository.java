package com.ongres.dvdstoreapi.domain.ports.repository;

import java.util.stream.Stream;

import javax.annotation.Nonnull;

import com.ongres.dvdstoreapi.domain.model.ActorFilm;

public interface FilmsRepository {

  @Nonnull Stream<ActorFilm> filmByActor(@Nonnull String actor);

}

package com.ongres.dvdstoreapi.infrastructure.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.function.Function;

import com.ongres.dvdstoreapi.domain.ports.repository.RepositoryException;

class ResultSetIterator<T> implements Iterator<T> {

  final Connection connection;
  final PreparedStatement statement;
  final ResultSet resultSet;
  final Function<ResultSet, T> mapper;

  ResultSetIterator(
      Connection connection,
      PreparedStatement statement,
      ResultSet resultSet,
      Function<ResultSet, T> mapper) {
    this.connection = connection;
    this.statement = statement;
    this.resultSet = resultSet;
    this.mapper = mapper;
  }

  @Override
  public boolean hasNext() {
    try {
      boolean next = !connection.isClosed()
          && !statement.isClosed()
          && !resultSet.isClosed() 
          && resultSet.next();
      if (!next) {
        if (!resultSet.isClosed()) {
          resultSet.close();
        }
        if (!statement.isClosed()) {
          statement.close();
        }
        if (!connection.isClosed()) {
          connection.close();
        }
      }
      return next;
    } catch (SQLException ex) {
      throw new RepositoryException(ex);
    }
  }

  @Override
  public T next() {
    return mapper.apply(resultSet);
  }
  
}

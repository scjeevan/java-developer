package com.ongres.dvdstoreapi.application.api;

import static org.hamcrest.Matchers.hasSize;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestRentalServiceIT extends AbstractRestServiceIT {

  @Autowired
  private WebTestClient webTestClient;

  @Test
  public void testOverdues() throws Exception {
    webTestClient.get()
        .uri("/rental/overdues")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBody()
        .jsonPath("$", hasSize(183));
  }

}
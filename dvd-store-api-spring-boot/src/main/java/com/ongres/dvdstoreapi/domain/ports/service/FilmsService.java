package com.ongres.dvdstoreapi.domain.ports.service;

import java.util.Objects;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.ongres.dvdstoreapi.domain.model.ActorFilm;
import com.ongres.dvdstoreapi.domain.ports.repository.FilmsRepository;

public class FilmsService {

  private final FilmsRepository filmsRepository;

  public FilmsService(FilmsRepository filmsRepository) {
    this.filmsRepository = filmsRepository;
  }

  public @Nonnull Stream<ActorFilm> filmByActorAndCategory(
      @Nonnull String actor,
      @Nullable String category) {
    return filmsRepository.filmByActor(actor)
        .filter(film -> category == null
          || Objects.equals(film.getFilmCategory(), category));
  }

}

package com.ongres.dvdstoreapi.infrastructure.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.ongres.dvdstoreapi.domain.model.RentalOverdue;
import com.ongres.dvdstoreapi.domain.ports.repository.RepositoryException;

class RentalOverdueResultSetMapper {
  final Integer[] columnIndexes;
  
  public RentalOverdueResultSetMapper(Integer...columnIndexes) {
    this.columnIndexes = columnIndexes;
  }

  RentalOverdue toRentalOverdue(ResultSet resultSet) {
    try {
      return RentalOverdue.builder()
          .customer(resultSet.getString(columnIndexes[0]))
          .phone(resultSet.getString(columnIndexes[1]))
          .title(resultSet.getString(columnIndexes[2]))
          .build();
    } catch (SQLException ex) {
      throw new RepositoryException(ex);
    }
  }
}

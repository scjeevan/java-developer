package com.ongres.dvdstoreapi.infrastructure.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.function.Function;

import com.ongres.dvdstoreapi.domain.ports.repository.RepositoryException;

/**
The ResultSetIterator class is an implementation of the Iterator interface that iterates over a ResultSet
and maps each row to an instance of a given type T using a provided Function.
*/
class ResultSetIterator<T> implements Iterator<T>, AutoCloseable {

	// The connection used to execute the query and retrieve the ResultSet.  
  final Connection connection;
  //The PreparedStatement used to execute the query and retrieve the ResultSet.
  final PreparedStatement statement;
  //The ResultSet being iterated over.
  final ResultSet resultSet;
  //The Function used to map each row of the ResultSet to an instance of the type T.
  final Function<ResultSet, T> mapper;

  /**
  Constructs a new ResultSetIterator instance with the given Connection, PreparedStatement, ResultSet and mapper.
  @param connection the Connection used to execute the query and retrieve the ResultSet.
  @param statement the PreparedStatement used to execute the query and retrieve the ResultSet.
  @param resultSet the ResultSet being iterated over.
  @param mapper the Function used to map each row of the ResultSet to an instance of the type T.
  */
  ResultSetIterator(
      Connection connection,
      PreparedStatement statement,
      ResultSet resultSet,
      Function<ResultSet, T> mapper) {
    this.connection = connection;
    this.statement = statement;
    this.resultSet = resultSet;
    this.mapper = mapper;
  }

  /**

  Returns true if there is at least one more row in the ResultSet to be iterated over, and false otherwise.
  Also closes the ResultSet, PreparedStatement and Connection if no more rows are available to be iterated.
  @return true if there is at least one more row in the ResultSet to be iterated over, and false otherwise.
  @throws RepositoryException if an SQLException is thrown while checking if there are more rows.
  */
  @Override
  public boolean hasNext() {
    try {
      boolean next = !connection.isClosed()
          && !statement.isClosed()
          && !resultSet.isClosed() 
          && resultSet.next();
      if (!next) {
        close();
      }
      return next;
    } catch (SQLException ex) {
      throw new RepositoryException(ex);
    }
  }

  /**

  Returns the next instance of the type T by applying the mapper Function to the next row of the ResultSet.
  @return the next instance of the type T.
  */
  @Override
  public T next() {
    return mapper.apply(resultSet);
  }

  /**
   * closes the ResultSet, PreparedStatement and Connection
   * @throws RepositoryException if an SQLException is thrown while closing resources.
   */
	@Override
	public void close(){
		try {
	      resultSet.close();
	      statement.close();
	      connection.close();
	    } catch (SQLException ex) {
	      throw new RepositoryException(ex);
	    }
	}
  
}

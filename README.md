# Java Developer

This is a technical test for Java Developers and is part of the hiring process at
 [OnGres](https://ongres.com). Plase, read this README carefully since it is
 a requirement in order to perform this test correctly.

Other requirements to perform this test are:

* A workstation connected to Internet that you will use to perform the test
* Your favorite Java development environment/tools ready to be used
* [Java JDK 17 or higher](https://jdk.java.net/archive/) installed on your workstation
* [Docker](https://docs.docker.com/get-docker/) installed on your workstation
* A [GitLab account](https://gitlab.com/users/sign_up) (If you do not own one. But if you
 are reading this you probably have one already!)

## What do we expect?

We expect that the amount of effort to do any of these exercises is in a few hours of actual work.
 We understand that your time is valuable, and in anyone's busy schedule that constitutes a fairly
 substantial chunk of time, so we really appreciate any effort you put into helping us build a
 solid team. For this reason this test is totally asynchronous and can be complete at any time
 within a time frame of 10 days starting from when this repository is cloned and a maximum of 3
 hours to complete the test. Do not worry if you are not able to solve some goals, the objective of
 this test is not only to prove you can perform a real task but to make sure your technical skills
 are aligned with what we are looking for and on how you perform it. For this reason there will
 always be a follow-up interview to the test were you will have the oportunity to discuss, defend
 or even fix your work.

## What we are looking for?

Keep it simple. Really. We really don't want you spending too much more time on it.

> Treat it like production code. But Keep it simple.

That is, develop your software in the same way that you would for any code that is intended to be
 deployed to production. These may be toy exercises, but we really would like to get an idea of how
 you build code on a day-to-day basis.

## How to submit?

The typical workflow we follow here at OnGres is to work on an issue ticket and create a branch to
 work on it. When the work is completed we create a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
 against the `main` branch in order to be reviewed by another colleague. The workflow we require you
 to follow have some modification due to some restrictions we need to apply in order to ensure your
 work is not shared with other candidates if you do not want to do so (see also the [#FAQ]
 section).

The workflow you have to follow is here divided into steps:

1. [Fork this project](https://gitlab.com/ongresinc/interview/java-developer/-/forks/new)) to a
 private project you own
2. Perform the test exercise on your workstation by [cloning your private project](https://docs.gitlab.com/ee/user/project/repository/#clone-a-repository)
 and push all the code to your private project in one or more branches
3. Create one or more [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
 in your private project against the `main` branch with the solution to the exercises
4. Fill the merge request description with any detail you may want to include and/or requested
 by the specific exercise.
5. [Invite to your private project](https://docs.gitlab.com/ee/user/project/members/) with Reporter
 role the following users: @jose_oss_pg @teoincontatto @jorsol @gelias
6. Send an email to `werehiring@ongres.com` with your name and the URL to your private repository.
 After reviewing your work we will schedule the follow-up interview.

## The Exercises

The main idea is to work over an existing project we developed. The project goal is to manage an
 hypothetical DVD Rental Store, that implements a REST API backed by a PostgreSQL database.

The [Sakila](https://www.jooq.org/sakila) example database model a DVD rental store and this
 project has been built around it. Check the [Use of Docker image](#use-of-docker-image) section
 to have a basic understanding of running and play with it yourself.

Few endpoints have been implemented for simplicity:

* `/customers/count-by-country/<country>?city=<city>`: Number of customers by the requested
 country (and optionally by city too). Provide results of type `application/json`. Returns a
 number.
* `/films/find-by-actor/<actor>?category=<category>`: Films by requested actor, with an optional
 filter by category. Provide results of type `application/json` and `application/x-ndjson`.
 Returns a list of objects with the following fields:
    * `actorFirstName`: Actor First Name.
    * `actorLastName`: Actor Last Name.
    * `filmTitle`: Film Title.
    * `filmDescription`: Film Description.
    * `categoryName`: Category Name.
* `/rental/overdues`: Daily list of overdue rentals so that customers can be contacted and asked
 to return their overdue DVDs. Provide results of type `application/json` and
 `application/x-ndjson`. Returns a list of objects with the following fields:
    * `customer`: Customer Last and First Name (separated by comma).
    * `phone`: Customer Phone.
    * `title`: Film Title.

We followed some technical requirements during implementation and we ask you to follow them
 while solving the exercises. Those requirements are:

  * The response must be a JSON.
  * To read from the database you must call JDBC API directly.

At OnGres we work with the [Quarkus](https://quarkus.io/) framework but we know that it is not
 very popular for Java developers as Spring Boot. For this reason we created two
 implementations of our DVD Rental Store REST API, one that uses Quarkus in the folder
 `dvd-store-api-quarkus` and anohter that uses [Spring Boot](https://spring.io/projects/spring-boot)
 framework in the folder `dvd-store-api-spring-boot`.
You are free to choose one of the two implementations you feel more confortable with or just
 go and try out the real OnGres experience by working on the Quarkus implementation. Choose
 one and stick with that for all the exercises. But now, get ready to code! 

### Exercise #1 - Bug hunter

#### Description

We know our codebase is bugfree... until someone find a bug! In this case a user issued a bug
 report that states the following:

> Hello! While searching the films for actor "DAN STREEP" I found that the film "WANDA CHAMBER"
> is repeated in the list when not filtered by category. This is unacceptable! Please, fix it.

#### Goals

* [ ] Taking into account that the database schema is perfectly fine, the
 bug, if there is any, must be in the code. Fix it.
* [ ] Add/modify the required tests if needed
* [ ] Document what was causing the problem and the solution implemented in the
 merge request

### Exercise #2 - Document and refactor

#### Description

Mike was a very smart developer that was working for our company some time ago. He developed the
 first application codebase and introduced this reactive code that involve transforming a
 `ResultSet` into a `Stream`, allowing to use the JSON Streaming on some REST API enpoints.
 Now that Mike has left none has the expertise and the confidence to look into his code and
 improve it.

We want you to document the class `com.ongres.dvdstoreapi.infrastructure.repository.ResultSetIterator`
 and the classes that uses or are user by it in order for the whole team to understand how it works
 and its pitfalls. After understanding the class we would like to know how and if it can be improved,
 for instance, for better usability, avoid duplication of code, refactor in a way that do not
 allow (or is harder) to use the code in the wrong way or any other improvement you can figure out.
 We would like the ideas to improve the code to be documented in the merge request and applied in the
 code.

#### Goals

* [ ] Document the class `com.ongres.dvdstoreapi.infrastructure.repository.ResultSetIterator`
 and the classes that uses or are user by it
* [ ] Analyze how to improve the code and document the ideas to improve the code in the merge request.
* [ ] Implement the ideas to improve the code.

### Exercise #3 - Tester

#### Description

We know a well written codebase should always have good test coverage but for this toy project
 as you can see we did not spent much effort on it by just providing some integration tests.

We believe the current approach is wrong since it seems more like a test ice-cream cone as
 mentioned in [this article](https://martinfowler.com/articles/practical-test-pyramid.html)
 about the test pyramid model were it is stated that testing in such a way is slow and hard to
 maintain. We should improve the testing strategy before the project start growing and it
 will require too much effort to change it.

#### Goals

* [ ] Document how you improved the testing strategy in the merge request
* [ ] Implement any change required in the code in order to apply the proposed strategy if
 required
* [ ] Add, change or remove tests following the proposed strategy

## Use of Docker image

We like Docker, so we use it (almost) everywere, this test is no exception :wink:. If you don't
 know how to use docker don't worry, only a few basic steps are required to be up and running.

This exercise use a docker image with PostgreSQL 11 and the [Sakila](https://www.jooq.org/sakila)
 database loaded (check the link to see the schema). The user, password and database name is
 `sakila`.

The project contains the `Dockerfile` to build the image in the `dvd-store-database` folder, so you
 can build it inside that folder running:
 
 ```
 docker build -t registry.gitlab.com/ongresinc/interview/java-developer/sakila .
 ```

If you prefer you can log into the registry `registry.gitlab.com` with your GitLab account and
 pull the image we already built for you:

```
docker login registry.gitlab.com
docker pull registry.gitlab.com/ongresinc/interview/java-developer/sakila
```

To play around with the database you can run the built image with:

```
docker run -d --rm --name dvd-store-database -p 5432 registry.gitlab.com/ongresinc/interview/java-developer/sakila
```

You will then be able to connect to the database with psql using the following command below: `crtl-d will
 close the psql client and bring you back to the command line`

```
docker exec -u 1000 -ti dvd-store-database psql
```

You may also connect to the running database using your favorite tool by connecting to the random
 port that is exposed by docker. To see the port value run the following command:

```
docker ps | grep ' dvd-store-database$'
```

And the output should be something similar to:

```
87569b940145   registry.gitlab.com/ongresinc/interview/java-developer/sakila   "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds   0.0.0.0:32794->5432/tcp, :::32794->5432/tcp        dvd-store-database
```

Where `32794` is the random port assigned by docker to the containerized PostgreSQL instance.

To stop the database run the command:

```
docker rm -fv dvd-store-database
```

## FAQ

*Do you have to complete all the tests?* Ideally yes, but don’t worry if you don’t have enough time
 to finish all the exercises. We’re going to value the clean code and the correctness of the
 solution.

*Is it OK to share your solutions publicly?* Yes, the questions are not prescriptive, the process
 and discussion around the code is the valuable part. You do the work, you own the code. We specify
 in the  Given we
 are asking you to give up your time, it is entirely reasonable for you to keep and use your
 solution as you see fit.

*Should I do X?* For any value of X, it is up to you, we will leave it up to you to provide us with
 what you see as important. Just remember the rough time frame of the project. If it is going to
 take you a couple of days, it isn't essential.

*Something is ambiguous, and I don't know what to do?* The first thing is: don't get stuck. We
 really don't want to trip you up intentionally, we are just attempting to see how you approach
 problems. If you really feel stuck, our first preference is for you to make a decision and
 document it with your submission. If you feel it is not possible to do this, just send us an email
 and we will try to clarify or correct the question for you.

### **Good luck!**
